#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char ***su;
void sudokuSolver();
void removePossibility(int,int,int);
void removePossib3x3(int,int,int);
void hiddenSingles();
void hidden3x3();
int main()
{   int i,j,k;
    su=(char***) malloc (sizeof(char**)*9);
    for(i=0; i<9; ++i)
    {   su[i]=(char**)malloc (sizeof(char*)*9);
        for(j=0; j<10; ++j)
        {   su[i][j]=(char*)malloc (sizeof(char)*11);
        }
    }
    printf("Input your sudoku\n");
    for(i=0; i<9; ++i)
    {   for(j=0; j<9; ++j)
        {   scanf("%s",&su[i][j][0]);
            if(su[i][j][0]=='0')
            {   for(k=0; k<10; ++k)
                {   su[i][j][k]=(char) (k+'0');
                }
                su[i][j][10]='9';
            }
            if(su[i][j][0]!='0')
            {   for(k=1; k<=10; ++k)
                {   su[i][j][k]='0';
                }
            }
        }
    }


    sudokuSolver();
    for(i=0; i<9; ++i)
    {   for(j=0; j<9; ++j)
        {   printf("%c ",su[i][j][0]);
        }
        printf("\n");
    }
    return 0;
}

void sudokuSolver()
{   int row1, col1;
    for(row1=0; row1<9; row1++)
    {   for(col1=0; col1<9; col1++)
        {   if(su[row1][col1][0]!='0')
            {   removePossibility(row1,col1,(int)(su[row1][col1][0]-'0'));
                //removePossib3x3(row1,col1,(int)(su[row1][col1][0]-'0'));
                //hiddenSingles();
            }

        }
    }
    hiddenSingles();
    hidden3x3();
}

void removePossibility(int row,int col,int val)
{   int counter,i;
    for(counter=0; counter<9; ++counter)
    {   /*checking row wise*/
        if((su[row][counter][val]!='0')&&(counter != col))
        {   su[row][counter][val]='0';
            su[row][counter][10]--;
        }
        if(su[row][counter][10]=='1')
        {   for(i=1; i<10; ++i)
            {   if(su[row][counter][i]!='0')
                {   su[row][counter][0]=su[row][counter][i];
                    su[row][counter][i]='0';
                    su[row][counter][10]='0';
                    break;
                }
            }
            removePossibility(row,counter,(int)(su[row][counter][0]-'0'));
        }
        /*checking column wise*/

        if((su[counter][col][val]!='0')&&(counter!=row))
        {   su[counter][col][val]='0';
            su[counter][col][10]--;
        }
        if(su[counter][col][10]=='1')
        {   for(i=1; i<10; ++i)
            {   if(su[counter][col][i]!='0')
                {   su[counter][col][0]=su[counter][col][i];
                    su[counter][col][i]='0';
                    su[counter][col][10]='0';
                    break;

                }
            }
            removePossibility(counter,col,(int)(su[counter][col][0]-'0'));
        }
    }
    removePossib3x3(row,col,val);
//    hiddenSingles();

}
void removePossib3x3(int row,int col,int val)
{   int i,j,l;
    if((row%3==0)&&(col%3==0))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i][col+j][val]!='0')&&( (row != row+i)||(col != col+j) ))
                {   su[row+i][col+j][val]='0';
                    su[row+i][col+j][10]--;
                }
                if((su[row+i][col+j][10]=='1')) /*upcoming fixed value*/
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i][col+j][l]!='0')
                        {   su[row+i][col+j][0]=su[row+i][col+j][l];
                            su[row+i][col+j][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i,col+j,(int)(su[row+i][col+j][0]-'0'));
                }
            }
        }
    }

    if((row%3==0)&&(col%3==1))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i][col+j-1][val]!='0')&&( (row != row+i)||(col != col+j-1) ))
                {   su[row+i][col+j-1][val]='0';
                    su[row+i][col+j-1][10]--;
                }
                if((su[row+i][col+j-1][10]=='1')/*&&((i%3 != 0)||(j%3 != 1))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i][col+j-1][l]!='0')
                        {   su[row+i][col+j-1][0]=su[row+i][col+j-1][l];
                            su[row+i][col+j-1][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i,col+j-1,(int)(su[row+i][col+j-1][0]-'0'));
                }
            }
        }
    }

    if((row%3==0)&&(col%3==2))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i][col+j-2][val]!='0')&&( (row != row+i)||(col != col+j-2) ))
                {   su[row+i][col+j-2][val]='0';
                    su[row+i][col+j-2][10]--;
                }
                if((su[row+i][col+j-2][10]=='1')/*&&((i%3 != 0)||(j%3 != 2))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i][col+j-2][l]!='0')
                        {   su[row+i][col+j-2][0]=su[row+i][col+j-2][l];
                            su[row+i][col+j-2][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i,col+j-2,(int)(su[row+i][col+j-2][0]-'0'));
                }
            }
        }
    }

    if((row%3==1)&&(col%3==0))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-1][col+j][val]!='0')&&( (row != row+i-1)||(col != col+j) ))
                {   su[row+i-1][col+j][val]='0';
                    su[row+i-1][col+j][10]--;
                }
                if((su[row+i-1][col+j][10]=='1')/*&&((i%3 != 1)||(j%3 != 0))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-1][col+j][l]!='0')
                        {   su[row+i-1][col+j][0]=su[row+i-1][col+j][l];
                            su[row+i-1][col+j][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-1,col+j,(int)(su[row+i-1][col+j][0]-'0'));
                }
            }
        }
    }

    if((row%3==1)&&(col%3==1))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-1][col+j-1][val]!='0')&&( (row != row+i-1)||(col != col+j-1) ))
                {   su[row+i-1][col+j-1][val]='0';
                    su[row+i-1][col+j-1][10]--;
                }
                if((su[row+i-1][col+j-1][10]=='1')/*&&((i%3 != 1)||(j%3 != 1))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-1][col+j-1][l]!='0')
                        {   su[row+i-1][col+j-1][0]=su[row+i-1][col+j-1][l];
                            su[row+i-1][col+j-1][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-1,col+j-1,(int)(su[row+i-1][col+j-1][0]-'0'));
                }
            }
        }
    }

    if((row%3==1)&&(col%3==2))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-1][col+j-2][val]!='0')&&( (row != row+i-1)||(col != col+j-2) ))
                {   su[row+i-1][col+j-2][val]='0';
                    su[row+i-1][col+j-2][10]--;
                }
                if((su[row+i-1][col+j-2][10]=='1')/*&&((i%3 != 1)||(j%3 != 2))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-1][col+j-2][l]!='0')
                        {   su[row+i-1][col+j-2][0]=su[row+i-1][col+j-2][l];
                            su[row+i-1][col+j-2][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-1,col+j-2,(int)(su[row+i-1][col+j-2][0]-'0'));
                }
            }
        }
    }

    if((row%3==2)&&(col%3==0))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-2][col+j][val]!='0')&&( (row != row+i-2)||(col != col+j) ))
                {   su[row+i-2][col+j][val]='0';
                    su[row+i-2][col+j][10]--;
                }
                if((su[row+i-2][col+j][10]=='1')/*&&((i%3 != 2)||(j%3 != 0))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-2][col+j][l]!='0')
                        {   su[row+i-2][col+j][0]=su[row+i-2][col+j][l];
                            su[row+i-2][col+j][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-2,col+j,(int)(su[row+i-2][col+j][0]-'0'));
                }
            }
        }
    }

    if((row%3==2)&&(col%3==1))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-2][col+j-1][val]!='0')&&( (row != row+i-2)||(col != col+j-1) ))
                {   su[row+i-2][col+j-1][val]='0';
                    su[row+i-2][col+j-1][10]--;
                }
                if((su[row+i-2][col+j-1][10]=='1')/*&&((i%3 != 2)||(j%3 != 1))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-2][col+j-1][l]!='0')
                        {   su[row+i-2][col+j-1][0]=su[row+i-2][col+j-1][l];
                            su[row+i-2][col+j-1][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-2,col+j-1,(int)(su[row+i-2][col+j-1][0]-'0'));
                }
            }
        }
    }

    if((row%3==2)&&(col%3==2))
    {
        for(i=0; i<3; ++i)
        {   for(j=0; j<3; ++j)
            {   if((su[row+i-2][col+j-2][val]!='0')&&( (row != row+i-2)||(col != col+j-2) ))
                {   su[row+i-2][col+j-2][val]='0';
                    su[row+i-2][col+j-2][10]--;
                }
                if((su[row+i-2][col+j-2][10]=='1')/*&&((i%3 != 2)||(j%3 != 2))*/)
                {   for(l=1; l<10; ++l)
                    {   if(su[row+i-2][col+j-2][l]!='0')
                        {   su[row+i-2][col+j-2][0]=su[row+i-2][col+j-2][l];
                            su[row+i-2][col+j-2][10]='0';
                            break;
                        }
                    }
                    removePossibility(row+i-2,col+j-2,(int)(su[row+i-2][col+j-2][0]-'0'));
                }
            }
        }
    }
}
void hiddenSingles()
{   int row,col,check,count,pos1,i ;

    for(row=0; row<9; row++)
    {
        for(check=1; check<=9; check++)
        {   count=0;
            for(col=0; col<9; col++)
            {
                if(su[row][col][0]=='0')
                {
                    if(su[row][col][check]==(char) (check+'0'))
                    {
                        count++;
                        pos1=col;
                    }
                }
            }
            if((count==1)&&(su[row][pos1][0]=='0'))
            {
                su[row][pos1][0]=(char)(check + '0');
                su[row][pos1][10]='0';
                for(i=1; i<=9; ++i)
                {   su[row][pos1][i]='0';
                }
                removePossibility(row,pos1,(int)(su[row][pos1][0]-'0'));

            }
        }
    }

    for(col=0; col<9; col++)
    {
        for(check=1; check<=9; check++)
        {   count=0;
            for(row=0; row<9; row++)
            {
                if(su[row][col][0]=='0')
                {
                    if(su[row][col][check]==(char) (check+'0'))
                    {
                        count++;
                        pos1=row;
                    }
                }
            }
            if((count==1)&&(su[pos1][col][0]=='0'))
            {
                su[pos1][col][0]=(char)(check+'0');
                su[pos1][col][10]='0';
                for(i=1; i<=9; ++i)
                {   su[pos1][col][i]='0';
                }

                removePossibility(pos1,col,(int)(su[pos1][col][0]-'0'));
            }
        }

    }
}


void hidden3x3()
{ int row,col,pos1,pos2,i,j,check,count;
    for(row=0; row<9; row++)
    {
        for(col=0; col<9; col++)
        {
            if((row%3==0)&&(col%3==0))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i][col+j][0]=='0')
                            {
                                if(su[row+i][col+j][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i;
                                    pos2=col+j;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
			for(i=1;i<=9;++i)
			{su[pos1][pos2][i]='0';
			}
                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }
            if((row%3==0)&&(col%3==1))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i][col+j-1][0]=='0')
                            {
                                if(su[row+i][col+j-1][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i;
                                    pos2=col+j-1;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }
            if((row%3==0)&&(col%3==2))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i][col+j-2][0]=='0')
                            {
                                if(su[row+i][col+j-2][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i;
                                    pos2=col+j-2;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
			hiddenSingles();
                    }
                }
            }
            if((row%3==1)&&(col%3==0))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-1][col+j][0]=='0')
                            {
                                if(su[row+i-1][col+j][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-1;
                                    pos2=col+j;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();

                    }
                }
            }
            if((row%3==1)&&(col%3==1))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-1][col+j-1][0]=='0')
                            {
                                if(su[row+i-1][col+j-1][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-1;
                                    pos2=col+j-1;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();

                    }
                }
            }
            if((row%3==1)&&(col%3==2))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-1][col+j-2][0]=='0')
                            {
                                if(su[row+i-1][col+j-2][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-1;
                                    pos2=col+j-2;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }
            if((row%3==2)&&(col%3==0))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-2][col+j][0]=='0')
                            {
                                if(su[row+i-2][col+j][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-2;
                                    pos2=col+j;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }
            if((row%3==2)&&(col%3==1))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-2][col+j-1][0]=='0')
                            {
                                if(su[row+i-2][col+j-1][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-2;
                                    pos2=col+j-1;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }
            if((row%3==2)&&(col%3==2))
            {
                for(check=1; check<=9; check++)
                {
                    count=0;
                    for(i=0; i<3; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(su[row+i-2][col+j-2][0]=='0')
                            {
                                if(su[row+i-2][col+j-2][check]==(char)(check+'0'))
                                {
                                    count++;
                                    pos1=row+i-2;
                                    pos2=col+j-2;
                                }
                            }
                        }
                    }
                    if(count==1)
                    {
                        su[pos1][pos2][0]=(char)(check+'0');
                        su[pos1][pos2][10]='0';
                        for(i=1;i<=9;++i)
                        {su[pos1][pos2][i]='0';
                        }

                        removePossibility(pos1,pos2,(int)(su[pos1][pos2][0]-'0'));
                        hiddenSingles();
                    }
                }
            }

        }
    }
}
